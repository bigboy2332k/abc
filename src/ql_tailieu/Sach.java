package ql_tailieu;

public class Sach extends TaiLieu {
	public String author;
	public int Page;

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public int getPage() {
		return Page;
	}

	public void setPage(int page) {
		Page = page;
	}

	public Sach() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Sach:" + this.ID + this.NXB + this.numsOfPulish + this.author + this.Page;
	}

}
