package ql_tailieu;

public class TaiLieu {
	public String ID;
	public String NXB;
	public int numsOfPulish;

	public TaiLieu() {
		super();
	}

	public TaiLieu(String ID, String NXB, int numsOfPublish) {
		super();
		this.ID = ID;
		this.NXB = NXB;
		this.numsOfPulish = numsOfPublish;
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getNXB() {
		return NXB;
	}

	public void setNXB(String nXB) {
		NXB = nXB;
	}

	public int getNumsOfPulish() {
		return numsOfPulish;
	}

	public void setNumsOfPulish(int numsOfPulish) {
		this.numsOfPulish = numsOfPulish;
	}

	public String toString() {
		return "Tai lieu:" + this.ID + "," + this.NXB + "," + this.numsOfPulish;
	}
}
