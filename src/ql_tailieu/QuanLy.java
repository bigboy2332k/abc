package ql_tailieu;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.*;
public class QuanLy {
	List<TaiLieu> Documents;	

	public QuanLy() {
		this.Documents = new ArrayList<>();
	}
    public void addDocument(TaiLieu t) {
    	this.Documents.add(t);
    }
    public boolean deleteDocument(String ID) {
//    	this.Documents.forEach(Documents->TaiLieu.getID().equal(ID));
//        TaiLieu doc= this.Documents.stream().filter(document->document.getID().equals(ID)).findFirst();
        TaiLieu doc = Documents.stream()
                .filter(document -> "ID".equals(document.getID()))
                .findFirst().orElse(null);

        if(doc == null) return false ;
        this.Documents.remove(doc);
		return true;
        
    }
    
    public void showInfo() {
    	this.Documents.forEach(Documents->System.out.println(Documents.toString()));
//       System.out.print(t.toString());
    }
    public void findBook() {
    	this.Documents.stream().filter(doc -> doc instanceof Sach).forEach(doc->System.out.print(doc.toString()));
    }
    
}
